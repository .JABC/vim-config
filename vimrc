
" Plugins Install
so ~/.vim/plugins.vim

" Plugins Config
so ~/.vim/plug-config/airline.vim
so ~/.vim/plug-config/emmet.vim

" Maps Key
so ~/.vim/maps.vim

" IDE
" so ~/.vim/ide-config.vim

" Colorscheme
set nocompatible
colorscheme onedark
set background=dark

syntax enable
filetype indent plugin on

set title  " Muestra el nombre del archivo en la ventana de la terminal
set number  " Muestra los números de las líneas
set mouse=a  " Permite la integración del mouse (seleccionar texto, mover el cursor)
set clipboard=unnamed
set showcmd
set ruler
set cursorline
set encoding=utf-8
set showmatch
set signcolumn=yes
set expandtab
set sw=2
set relativenumber
set laststatus=0
set noshowmode
set hidden

set cursorline  " Resalta la línea actual
set colorcolumn=120  " Muestra la columna límite a 120 caracteres

set autoindent

" Indentación a 4 espacios
set tabstop=4
set shiftwidth=4
set softtabstop=4
set shiftround
set expandtab  " Insertar espacios en lugar de <Tab>s

set termguicolors

"" Searching
set hlsearch                    " highlight matches
set incsearch                   " incremental searching
set ignorecase  " Ignorar mayúsculas al hacer una búsqueda
set smartcase  " No ignorar mayúsculas si la palabra a buscar contiene mayúscula

set backspace=indent,eol,start
set nowrap
set wildmenu
