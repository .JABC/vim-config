
call plug#begin('~/.vim/plugged')
    
    " Status bar
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    
    " Theme
    Plug 'joshdick/onedark.vim'
    
    " Typing
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-commentary'
    Plug 'AndrewRadev/tagalong.vim'
    Plug 'alvan/vim-closetag'
    Plug 'jiangmiao/auto-pairs'   

    " IDE
    Plug 'easymotion/vim-easymotion'
    Plug 'christoomey/vim-tmux-navigator'
    Plug 'mattn/emmet-vim'
    Plug 'yggdroot/indentline'
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'mg979/vim-visual-multi'

    " Git
    Plug 'airblade/vim-gitgutter'
    
    " Icons
    Plug 'ryanoasis/vim-devicons'

call plug#end()



